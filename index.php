<?php
include 'pdo.php';
require_once 'class/encode.php';

//header('Access-Control-Allow-Origin:*');

$encode = new \encode\encode();
$a = '';
if (isset($_POST['submit']) && $_POST['submit']) {
	$username = $_POST['username'];
	$userpass = $_POST['userpass'];
	$username = htmlspecialchars($username);
	$userpass = htmlspecialchars($userpass);
	$sql = "select * from userinfo where username = ? and userpass = ?";
	$stmt = $dblj->prepare($sql);
	$bool = $stmt->execute(array($username, $userpass));
	$stmt->bindColumn('username', $cxusername);
	$stmt->bindColumn('userpass', $cxuserpass);
	$stmt->bindColumn('token', $cxtoken);
	$exeres = $stmt->fetch(PDO::FETCH_ASSOC);
	if ((strlen($username) < 6 || strlen($userpass) < 6) && !$exeres) {
		$a = '账号或密码错误';
	} elseif ($cxusername == $username && $cxuserpass == $userpass) {
		$sql = "select * from game1 where token='$cxtoken'";
		$cxjg = $dblj->query($sql);
		$cxjg->bindColumn('sid', $sid);
		$cxjg->fetch(PDO::FETCH_ASSOC);
		if ($sid == null) {
			$cmd = "cmd=cj&token=$cxtoken";
		} else {
			$cmd = "cmd=login&sid=$sid";
			$nowdate = date('Y-m-d H:i:s');
			$sql = "update game1 set endtime = '$nowdate',sfzx=1 WHERE sid=?";
			$stmt = $dblj->prepare($sql);
			$stmt->execute(array($sid));
		}
		// $cmd = $encode->encode($cmd);
		// print_r($cmd);
		header("refresh:1;url=game.php?$cmd");
	}
}
?>
<html lang="zh">

<head>
	<meta charset="utf-8" content="width=device-width,user-scalable=no" name="viewport" />
	<title>紫鼎圣宫</title>
	<link href="//unpkg.com/layui@2.8.8/dist/css/layui.css" rel="stylesheet">
	<!-- <link href="https://cdn.bootcdn.net/ajax/libs/layui/2.8.8/css/layui.min.css" rel="stylesheet"> -->
	<script src="//unpkg.com/layui@2.8.8/dist/layui.js"></script>
	<!-- <script src="https://cdn.bootcdn.net/ajax/libs/layui/2.8.8/layui.min.js"></script> -->
	<link rel="stylesheet" href="css/gamecss.css?version=1.01">
	<style>
		.demo-login-other .layui-icon {
			position: relative;
			display: inline-block;
			margin: 0 2px;
			top: 2px;
			font-size: 26px;
		}
	</style>
</head>

<body>

	<div class="layui-container">
		<div class="layui-card">
			<div class="layui-card-body">
				<img src="images/11.jpg" width="100%" height="200">
				<br /><br />
				<div id="mainfont">
					月冷千山江自碧，冰崖万丈无留意。<br />
					寻道只影莲花落，竹音寥落听新曲。<br />
					仙人听谁醉明月，踏浪踏风随燕去。<br />
					纪纲人伦心如桑，一醉红尘消百绪。<br />
					魔前扣首三千年，回首红尘不做仙。<br />
				</div>
				<br>
				<form action="<?php echo $_SERVER['PHP_SELF'] ?>" method="post" class="layui-form">
					<div class="layui-form-item">
						<div class="layui-input-wrap">
							<div class="layui-input-prefix">
								<i class="layui-icon layui-icon-username"></i>
							</div>
							<input type="text" name="username" value="" lay-verify="required" placeholder="账号" lay-reqtext="请填写账号" autocomplete="off" class="layui-input" lay-affix="clear">
						</div>
					</div>
					<div class="layui-form-item">
						<div class="layui-input-wrap">
							<div class="layui-input-prefix">
								<i class="layui-icon layui-icon-password"></i>
							</div>
							<input type="password" name="userpass" value="" lay-verify="required" placeholder="密码" lay-reqtext="请填写密码" autocomplete="off" class="layui-input" lay-affix="eye">
						</div>
					</div>
					<!-- <div class="layui-form-item">
							<div class="layui-row">
								<div class="layui-col-xs7">
									<div class="layui-input-wrap">
										<div class="layui-input-prefix">
											<i class="layui-icon layui-icon-vercode"></i>
										</div>
										<input type="text" name="captcha" value="" lay-verify="required" placeholder="验证码" lay-reqtext="请填写验证码" autocomplete="off" class="layui-input" lay-affix="clear">
									</div>
								</div>
								<div class="layui-col-xs5">
									<div style="margin-left: 10px;">
										<img src="https://www.oschina.net/action/user/captcha" onclick="this.src='https://www.oschina.net/action/user/captcha?t='+ new Date().getTime();">
									</div>
								</div>
							</div>
						</div> -->
					<div class="layui-form-item">
						<input type="checkbox" name="remember" lay-skin="primary" title="记住密码">
						<a href="#forget" style="float: right; margin-top: 7px;">忘记密码？</a>
					</div>
					<div class="layui-form-item">
						<input class="layui-btn layui-btn-fluid" type="submit" name="submit" value="登陆">
						<!-- <button class="layui-btn layui-btn-fluid" lay-submit lay-filter="demo-login">登录</button> -->
					</div>
					<div class="layui-form-item demo-login-other">
						<label>社交账号登录</label>
						<span style="padding: 0 21px 0 6px;">
							<a href="javascript:;"><i class="layui-icon layui-icon-login-qq" style="color: #3492ed;"></i></a>
							<a href="javascript:;"><i class="layui-icon layui-icon-login-wechat" style="color: #4daf29;"></i></a>
							<a href="javascript:;"><i class="layui-icon layui-icon-login-weibo" style="color: #cf1900;"></i></a>
						</span>
						或 <a href="reguser.php">注册帐号</a>
					</div>
				</form>






				<!-- 
				<form action=<?php echo $_SERVER['PHP_SELF']; ?> method="post">
					账号:<br />
					<input type="text" name="username"><br />
					密码:<br />
					<input type="password" name="userpass"><br />
					<?php echo $a ?>
					<p><input type="submit" name="submit" value="登陆"> <a href="reguser.php" id="btn">注册</a></p>
				</form> -->
				<footer>
					<?php echo date('Y-m-d H:i:s') ?>
				</footer>
			</div>
		</div>
	</div>
	<script>
		// layui.use(function() {
		// 	var form = layui.form;
		// 	var layer = layui.layer;
		// 	// 提交事件
		// 	form.on('submit(demo-login)', function(data) {
		// 		var field = data.field; // 获取表单字段值
		// 		// 显示填写结果，仅作演示用
		// 		layer.alert(JSON.stringify(field), {
		// 			title: '当前填写的字段值'
		// 		});
		// 		// 此处可执行 Ajax 等操作
		// 		// …
		// 		return false; // 阻止默认 form 跳转
		// 	});
		// });
	</script>
</body>


</html>





<!-- <!DOCTYPE html>
<html>

<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Demo</title>
	<link href="//unpkg.com/layui@2.8.8/dist/css/layui.css" rel="stylesheet">
</head>

<body>
	<style>
		.demo-login-container {
			width: 320px;
			margin: 21px auto 0;
		}

		.demo-login-other .layui-icon {
			position: relative;
			display: inline-block;
			margin: 0 2px;
			top: 2px;
			font-size: 26px;
		}
	</style>
	<form class="layui-form">
		<div class="demo-login-container">
			<div class="layui-form-item">
				<div class="layui-input-wrap">
					<div class="layui-input-prefix">
						<i class="layui-icon layui-icon-username"></i>
					</div>
					<input type="text" name="username" value="" lay-verify="required" placeholder="用户名" lay-reqtext="请填写用户名" autocomplete="off" class="layui-input" lay-affix="clear">
				</div>
			</div>
			<div class="layui-form-item">
				<div class="layui-input-wrap">
					<div class="layui-input-prefix">
						<i class="layui-icon layui-icon-password"></i>
					</div>
					<input type="password" name="password" value="" lay-verify="required" placeholder="密   码" lay-reqtext="请填写密码" autocomplete="off" class="layui-input" lay-affix="eye">
				</div>
			</div>
			<div class="layui-form-item">
				<div class="layui-row">
					<div class="layui-col-xs7">
						<div class="layui-input-wrap">
							<div class="layui-input-prefix">
								<i class="layui-icon layui-icon-vercode"></i>
							</div>
							<input type="text" name="captcha" value="" lay-verify="required" placeholder="验证码" lay-reqtext="请填写验证码" autocomplete="off" class="layui-input" lay-affix="clear">
						</div>
					</div>
					<div class="layui-col-xs5">
						<div style="margin-left: 10px;">
							<img src="https://www.oschina.net/action/user/captcha" onclick="this.src='https://www.oschina.net/action/user/captcha?t='+ new Date().getTime();">
						</div>
					</div>
				</div>
			</div>
			<div class="layui-form-item">
				<input type="checkbox" name="remember" lay-skin="primary" title="记住密码">
				<a href="#forget" style="float: right; margin-top: 7px;">忘记密码？</a>
			</div>
			<div class="layui-form-item">
				<button class="layui-btn layui-btn-fluid" lay-submit lay-filter="demo-login">登录</button>
			</div>
			<div class="layui-form-item demo-login-other">
				<label>社交账号登录</label>
				<span style="padding: 0 21px 0 6px;">
					<a href="javascript:;"><i class="layui-icon layui-icon-login-qq" style="color: #3492ed;"></i></a>
					<a href="javascript:;"><i class="layui-icon layui-icon-login-wechat" style="color: #4daf29;"></i></a>
					<a href="javascript:;"><i class="layui-icon layui-icon-login-weibo" style="color: #cf1900;"></i></a>
				</span>
				或 <a href="#reg">注册帐号</a>
			</div>
		</div>
	</form>

	<script src="//unpkg.com/layui@2.8.8/dist/layui.js"></script>
	<script>
		layui.use(function() {
			var form = layui.form;
			var layer = layui.layer;
			// 提交事件
			form.on('submit(demo-login)', function(data) {
				var field = data.field; // 获取表单字段值
				// 显示填写结果，仅作演示用
				layer.alert(JSON.stringify(field), {
					title: '当前填写的字段值'
				});
				// 此处可执行 Ajax 等操作
				// …
				return false; // 阻止默认 form 跳转
			});
		});
	</script>
</body>

</html> -->