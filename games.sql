/*
Navicat MySQL Data Transfer

Source Server         : 寻仙
Source Server Version : 50547
Source Host           : 127.0.0.1:3306
Source Database       : game

Target Server Type    : MYSQL
Target Server Version : 50547
File Encoding         : 65001

Date: 2017-01-08 14:26:16
*/

-- SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for boss
-- ----------------------------
DROP TABLE IF EXISTS `boss`;
CREATE TABLE `boss` (
  `bossname` text NOT NULL,
  `bossinfo` text NOT NULL,
  `bosslv` text NOT NULL,
  `bosshp` varchar(255) NOT NULL,
  `bossmaxhp` varchar(255) NOT NULL,
  `bossgj` varchar(255) NOT NULL,
  `bossfy` varchar(255) NOT NULL,
  `bossbj` varchar(255) NOT NULL,
  `bossxx` varchar(255) NOT NULL,
  `bosszb` varchar(255) NOT NULL,
  `bossdj` varchar(255) NOT NULL,
  `bossid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `bs` int(255) NOT NULL,
  `bosstime` datetime NOT NULL,
  PRIMARY KEY (`bossid`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of boss
-- ----------------------------

-- ----------------------------
-- Table structure for club
-- ----------------------------
DROP TABLE IF EXISTS `club`;
CREATE TABLE `club` (
  `clubname` varchar(255) NOT NULL,
  `clubinfo` varchar(255) NOT NULL,
  `clublv` varchar(255) NOT NULL,
  `clubid` int(11) NOT NULL,
  `clubno1` int(11) NOT NULL,
  `clubexp` int(11) NOT NULL,
  `clubyxb` int(11) NOT NULL,
  `clubczb` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of club
-- ----------------------------

-- ----------------------------
-- Table structure for clubplayer
-- ----------------------------
DROP TABLE IF EXISTS `clubplayer`;
CREATE TABLE `clubplayer` (
  `clubid` int(11) NOT NULL,
  `sid` varchar(255) NOT NULL,
  `uid` int(11) NOT NULL,
  `uclv` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of clubplayer
-- ----------------------------
INSERT INTO `clubplayer` VALUES ('0', '087efeb819f49c1789df1f599ec15388', '419', '1');

-- ----------------------------
-- Table structure for daoju
-- ----------------------------
DROP TABLE IF EXISTS `daoju`;
CREATE TABLE `daoju` (
  `djname` varchar(255) NOT NULL,
  `djzl` varchar(255) NOT NULL,
  `djinfo` varchar(255) NOT NULL,
  `djid` int(11) NOT NULL AUTO_INCREMENT,
  `djyxb` int(11) NOT NULL,
  PRIMARY KEY (`djid`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of daoju
-- ----------------------------
INSERT INTO `daoju` VALUES ('强化石', '2', '强化装备用的道具', '1', '0');
INSERT INTO `daoju` VALUES ('符箓残页-初级灵', '', '兑换符箓用的', '6', '0');
INSERT INTO `daoju` VALUES ('符箓残页-初级魔', '', '兑换符箓', '7', '0');
INSERT INTO `daoju` VALUES ('硬翅蜂蜜', '', '硬翅蜂的蜂蜜', '8', '0');
INSERT INTO `daoju` VALUES ('符箓残页-初级蛮', '', '符箓残页-初级蛮', '9', '0');
INSERT INTO `daoju` VALUES ('符箓残页-中级灵', '', '符箓残页-中级灵', '10', '0');
INSERT INTO `daoju` VALUES ('初级魔灵', '', '初级魔灵', '11', '0');
INSERT INTO `daoju` VALUES ('[神器]妖王剑碎片', '', '[神器]妖王剑碎片', '12', '0');

-- ----------------------------
-- Table structure for duihuan
-- ----------------------------
DROP TABLE IF EXISTS `duihuan`;
CREATE TABLE `duihuan` (
  `dhm` varchar(255) NOT NULL,
  `dhzb` varchar(255) DEFAULT NULL,
  `dhdj` varchar(255) DEFAULT NULL,
  `dhyp` varchar(255) DEFAULT NULL,
  `dhyxb` int(11) NOT NULL,
  `dhczb` int(11) NOT NULL,
  `dhname` varchar(255) DEFAULT NULL,
  `dhexp` int(11) NOT NULL,
  `dhid` int(11) unsigned NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`dhid`,`dhm`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of duihuan
-- ----------------------------
INSERT INTO `duihuan` VALUES ('49852B2FA355EA54', '33', '1|100', '9|100', '0', '0', '新手升级包', '88888', '1');
INSERT INTO `duihuan` VALUES ('DA71AAF69D931648', '33', '1|100', '9|100', '0', '0', '新手升级包', '88888', '1');
INSERT INTO `duihuan` VALUES ('2B5BAECC1CBA455C', '33', '1|100', '9|100', '0', '0', '新手升级包', '88888', '1');
INSERT INTO `duihuan` VALUES ('C2BAF2D5ADF0C03E', '33', '1|100', '9|100', '0', '0', '新手升级包', '88888', '1');
INSERT INTO `duihuan` VALUES ('870C85455682BC80', '33', '1|100', '9|100', '0', '0', '新手升级包', '88888', '1');
INSERT INTO `duihuan` VALUES ('E3E4ED0CD757A3CF', '33', '1|100', '9|100', '0', '0', '新手升级包', '88888', '1');
INSERT INTO `duihuan` VALUES ('8048AE4F8EF869EE', '33', '1|100', '9|100', '0', '0', '新手升级包', '88888', '1');
INSERT INTO `duihuan` VALUES ('0D959B010FF1EF9D', '33', '1|100', '9|100', '0', '0', '新手升级包', '88888', '1');
INSERT INTO `duihuan` VALUES ('240C93CAEA5F1FA9', '33', '1|100', '9|100', '0', '0', '新手升级包', '88888', '1');

-- ----------------------------
-- Table structure for exp
-- ----------------------------
DROP TABLE IF EXISTS `exp`;
CREATE TABLE `exp` (
  `ulv` text NOT NULL,
  `uexp` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of exp
-- ----------------------------

-- ----------------------------
-- Table structure for fangshi_dj
-- ----------------------------
DROP TABLE IF EXISTS `fangshi_dj`;
CREATE TABLE `fangshi_dj` (
  `djid` int(11) NOT NULL,
  `djcount` int(11) NOT NULL,
  `uid` int(11) NOT NULL,
  `pay` int(11) NOT NULL,
  `payid` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `djname` varchar(255) NOT NULL,
  PRIMARY KEY (`payid`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of fangshi_dj
-- ----------------------------
INSERT INTO `fangshi_dj` VALUES ('1', '10', '252', '11', '1', '强化石');

-- ----------------------------
-- Table structure for fangshi_zb
-- ----------------------------
DROP TABLE IF EXISTS `fangshi_zb`;
CREATE TABLE `fangshi_zb` (
  `zbnowid` int(11) NOT NULL,
  `zbname` varchar(255) NOT NULL,
  `qianghua` int(11) NOT NULL,
  `pay` int(11) NOT NULL,
  `payid` int(11) NOT NULL,
  `zbinfo` varchar(255) NOT NULL,
  `zbgj` int(11) NOT NULL,
  `zbfy` int(11) NOT NULL,
  `zbbj` int(11) NOT NULL,
  `zbxx` int(11) NOT NULL,
  `zbid` int(11) NOT NULL,
  `uid` int(11) NOT NULL,
  `sid` int(11) NOT NULL,
  `zbhp` int(11) NOT NULL,
  `zblv` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of fangshi_zb
-- ----------------------------
INSERT INTO `fangshi_zb` VALUES ('75616', '初级嗜血剑', '0', '13', '0', '初级嗜血剑', '2', '0', '1', '3', '29', '419', '87', '0', '0');
INSERT INTO `fangshi_zb` VALUES ('75612', '初级嗜血剑', '0', '14', '0', '初级嗜血剑', '2', '0', '1', '3', '29', '419', '87', '0', '0');
INSERT INTO `fangshi_zb` VALUES ('75621', '百炼轻蕊甲', '0', '8888', '0', '百炼轻蕊甲', '0', '8', '0', '0', '28', '419', '87', '40', '0');

-- ----------------------------
-- Table structure for fb
-- ----------------------------
DROP TABLE IF EXISTS `fb`;
CREATE TABLE `fb` (
  `fbname` varchar(255) NOT NULL,
  `fbid` int(11) NOT NULL,
  `fbinfo` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`fbid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of fb
-- ----------------------------
INSERT INTO `fb` VALUES ('测试副本', '0', '测试用的副本');

-- ----------------------------
-- Table structure for fbmid
-- ----------------------------
DROP TABLE IF EXISTS `fbmid`;
CREATE TABLE `fbmid` (
  `fmname` varchar(255) NOT NULL,
  `fmid` int(11) NOT NULL,
  `fminfo` varchar(255) NOT NULL,
  `fmnpc` varchar(255) NOT NULL,
  `fmgw` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of fbmid
-- ----------------------------

-- ----------------------------
-- Table structure for game1
-- ----------------------------
DROP TABLE IF EXISTS `game1`;
CREATE TABLE `game1` (
  `uid` int(11) NOT NULL AUTO_INCREMENT,
  `sid` text CHARACTER SET utf8 NOT NULL,
  `token` text CHARACTER SET utf8 NOT NULL,
  `uname` text CHARACTER SET utf8 NOT NULL,
  `ulv` int(10) unsigned NOT NULL DEFAULT '1',
  `uyxb` int(11) NOT NULL DEFAULT '2000',
  `uczb` int(11) NOT NULL DEFAULT '100',
  `uexp` int(11) NOT NULL DEFAULT '0',
  `vip` int(11) NOT NULL DEFAULT '0',
  `uhp` int(11) NOT NULL DEFAULT '35',
  `umaxhp` int(11) NOT NULL DEFAULT '35',
  `ugj` int(11) NOT NULL DEFAULT '12',
  `ufy` int(11) NOT NULL DEFAULT '5',
  `usex` int(11) NOT NULL DEFAULT '1',
  `endtime` datetime NOT NULL,
  `nowmid` int(11) NOT NULL DEFAULT '225',
  `uwx` int(11) NOT NULL DEFAULT '0',
  `nowguaiwu` int(11) NOT NULL,
  `tool1` int(11) NOT NULL,
  `tool2` int(11) NOT NULL,
  `tool3` int(11) NOT NULL,
  `tool4` int(11) NOT NULL,
  `tool5` int(11) NOT NULL,
  `tool6` int(11) NOT NULL,
  `ubj` int(11) NOT NULL DEFAULT '0',
  `uxx` int(11) NOT NULL DEFAULT '0',
  `sfzx` int(11) NOT NULL DEFAULT '0',
  `qandaotime` datetime NOT NULL,
  `xiuliantime` datetime NOT NULL,
  `sfxl` int(11) NOT NULL DEFAULT '0',
  `yp1` int(11) NOT NULL,
  `yp2` int(11) NOT NULL,
  `yp3` int(11) NOT NULL,
  `cw` int(11) NOT NULL,
  `jn1` int(11) NOT NULL,
  `jn2` int(11) NOT NULL,
  `jn3` int(11) NOT NULL,
  `ispvp` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`uid`)
) ENGINE=InnoDB AUTO_INCREMENT=422 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of game1
-- ----------------------------

-- ----------------------------
-- Table structure for gameconfig
-- ----------------------------
DROP TABLE IF EXISTS `gameconfig`;
CREATE TABLE `gameconfig` (
  `firstmid` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of gameconfig
-- ----------------------------
INSERT INTO `gameconfig` VALUES ('225');

-- ----------------------------
-- Table structure for ggliaotian
-- ----------------------------
DROP TABLE IF EXISTS `ggliaotian`;
CREATE TABLE `ggliaotian` (
  `name` text NOT NULL,
  `msg` text CHARACTER SET utf8mb4 NOT NULL,
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `uid` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4054 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of ggliaotian
-- ----------------------------

-- ----------------------------
-- Table structure for guaiwu
-- ----------------------------
DROP TABLE IF EXISTS `guaiwu`;
CREATE TABLE `guaiwu` (
  `gname` text CHARACTER SET utf8 NOT NULL,
  `glv` text NOT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ginfo` text CHARACTER SET utf8 NOT NULL,
  `gsex` varchar(255) NOT NULL,
  `ghp` int(11) NOT NULL,
  `ggj` int(11) NOT NULL,
  `gfy` int(11) NOT NULL,
  `gbj` int(11) NOT NULL,
  `gxx` int(11) NOT NULL,
  `gzb` text NOT NULL,
  `dljv` int(11) NOT NULL,
  `gdj` text NOT NULL,
  `djjv` int(11) NOT NULL,
  `gyp` text NOT NULL,
  `ypjv` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=99 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of guaiwu
-- ----------------------------
INSERT INTO `guaiwu` VALUES ('硬翅蜂', '1', '55', '硬翅蜂', '男', '40', '5', '3', '0', '0', '23', '10', '8', '30', '6', '8');
INSERT INTO `guaiwu` VALUES ('山猪', '1', '56', '山上的诸', '男', '40', '4', '4', '0', '0', '24', '10', '1', '2', '', '0');
INSERT INTO `guaiwu` VALUES ('老虎', '2', '57', '凶猛的老虎', '公', '70', '8', '6', '0', '0', '25', '10', '1', '3', '', '0');
INSERT INTO `guaiwu` VALUES ('花妖', '3', '58', '花妖,小妖', '女', '110', '15', '7', '0', '0', '26', '10', '1', '4', '', '0');
INSERT INTO `guaiwu` VALUES ('狂暴野狼', '6', '62', '狂暴野狼', '男', '190', '23', '19', '0', '0', '28', '20', '1', '5', '', '0');
INSERT INTO `guaiwu` VALUES ('嗜血野狼', '5', '61', '嗜血野狼', '男', '160', '22', '16', '0', '0', '28', '20', '1', '5', '', '0');
INSERT INTO `guaiwu` VALUES ('龙雀', '7', '63', '龙雀', '女', '220', '27', '22', '0', '0', '27', '20', '1,6', '8', '', '0');
INSERT INTO `guaiwu` VALUES ('百岁龙雀', '8', '64', '百岁龙雀', '男', '250', '32', '25', '0', '0', '29', '20', '1,6', '7', '', '0');
INSERT INTO `guaiwu` VALUES ('荷花花魅', '9', '65', '荷花花魅', '女', '280', '35', '28', '0', '0', '30', '21', '6', '17', '', '0');
INSERT INTO `guaiwu` VALUES ('血雷鹰', '12', '66', '血雷鹰', '男', '370', '46', '37', '0', '0', '32', '21', '1', '6', '', '0');
INSERT INTO `guaiwu` VALUES ('雷鹰', '10', '67', '雷鹰', '男', '310', '38', '31', '0', '0', '31', '21', '1', '5', '', '0');
INSERT INTO `guaiwu` VALUES ('魔修士', '13', '69', '魔修士', '男', '400', '49', '40', '0', '0', '', '22', '1', '6', '', '0');
INSERT INTO `guaiwu` VALUES ('魔化之通臂猿', '16', '70', '魔化之通臂猿', '男', '490', '61', '50', '0', '0', '36', '22', '', '5', '6', '5');
INSERT INTO `guaiwu` VALUES ('魔化之灵猴', '17', '71', '魔化之灵猴', '男', '520', '65', '53', '0', '0', '', '5', '1', '5', '', '5');
INSERT INTO `guaiwu` VALUES ('魔化之剑魔', '18', '72', '魔化之剑魔', '男', '550', '68', '56', '0', '0', '', '5', '7', '20', '', '5');
INSERT INTO `guaiwu` VALUES ('魔化之幼齿猫女', '19', '73', '魔化之幼齿猫女', '女', '580', '72', '59', '0', '0', '37', '5', '1,7', '5', '7', '5');
INSERT INTO `guaiwu` VALUES ('魔化之鸦人', '20', '74', '魔化之鸦人', '男', '610', '76', '62', '0', '0', '', '5', '7', '5', '', '5');
INSERT INTO `guaiwu` VALUES ('魔化山魈', '21', '75', '魔化山魈', '男', '640', '80', '65', '0', '0', '33', '20', '1,7', '20', '7', '20');
INSERT INTO `guaiwu` VALUES ('入魔蛮族战士', '21', '76', '入魔的蛮族战士', '男', '640', '80', '65', '0', '0', '34,35', '7', '1', '7', '7', '7');
INSERT INTO `guaiwu` VALUES ('入魔蛮族祭祀', '22', '77', '入魔的蛮族祭祀', '女', '670', '84', '68', '0', '0', '35', '7', '1', '7', '7', '7');
INSERT INTO `guaiwu` VALUES ('魔化蛮狼', '23', '78', '魔化的蛮狼', '男', '700', '87', '71', '0', '0', '', '7', '1', '6', '', '7');
INSERT INTO `guaiwu` VALUES ('魔化狂狮', '24', '79', '魔化狂狮', '男', '730', '91', '74', '0', '0', '', '7', '1', '6', '', '7');
INSERT INTO `guaiwu` VALUES ('魔道控兽师', '26', '80', '', '男', '790', '99', '81', '0', '0', '', '7', '1', '6', '', '7');
INSERT INTO `guaiwu` VALUES ('百年蛇怪', '27', '81', '修炼百年的蛇怪', '男', '820', '103', '84', '0', '0', '', '7', '1', '8', '', '7');
INSERT INTO `guaiwu` VALUES ('赤鳞兽', '28', '82', '赤鳞兽', '男', '850', '106', '87', '0', '0', '', '7', '1,10', '7', '', '7');
INSERT INTO `guaiwu` VALUES ('棘鼠', '29', '83', '棘鼠', '男', '880', '110', '90', '0', '0', '38', '7', '9', '20', '', '7');
INSERT INTO `guaiwu` VALUES ('怨灵', '30', '84', '怨灵', '女', '1510', '204', '183', '0', '0', '39', '8', '1', '9', '', '8');
INSERT INTO `guaiwu` VALUES ('岩魔', '31', '85', '岩魔', '男', '1560', '211', '189', '0', '0', '40', '8', '1', '8', '', '8');
INSERT INTO `guaiwu` VALUES ('嗜灵鼠妖', '32', '86', '嗜灵鼠妖', '男', '1610', '218', '195', '0', '0', '41', '8', '1', '9', '', '8');
INSERT INTO `guaiwu` VALUES ('赤炼蝮蛇妖', '33', '87', '赤炼蝮蛇妖', '女', '1660', '224', '201', '0', '0', '42', '9', '1', '9', '8', '9');
INSERT INTO `guaiwu` VALUES ('百年穿山甲', '34', '88', '百年穿山甲\r\n皮厚气血强盛', '男', '2000', '150', '330', '0', '0', '44', '9', '1', '9', '8', '9');
INSERT INTO `guaiwu` VALUES ('青古虎', '35', '89', '青古虎', '男', '1760', '238', '214', '0', '0', '', '5', '1', '5', '', '5');
INSERT INTO `guaiwu` VALUES ('烈珠鹰', '37', '90', '烈珠鹰', '男', '1860', '252', '226', '0', '0', '', '5', '1', '5', '9', '5');
INSERT INTO `guaiwu` VALUES ('白豹妖', '38', '91', '白豹妖', '男', '1910', '258', '232', '0', '0', '43', '5', '1', '5', '', '5');
INSERT INTO `guaiwu` VALUES ('雷萝妖', '39', '92', '雷萝妖', '女', '1960', '265', '238', '0', '0', '', '5', '1', '5', '', '5');
INSERT INTO `guaiwu` VALUES ('血鸦', '40', '93', '血鸦', '男', '2010', '272', '244', '0', '0', '', '5', '1', '10', '', '5');
INSERT INTO `guaiwu` VALUES ('血鸦王', '55', '94', '血鸦王', '男', '5860', '594', '558', '0', '0', '', '8', '12', '1', '', '8');
INSERT INTO `guaiwu` VALUES ('黑艳妖王', '56', '95', '黑艳妖王', '女', '4530', '653', '454', '0', '0', '', '5', '12', '1', '', '5');
INSERT INTO `guaiwu` VALUES ('劫杀者', '45', '96', '劫杀者', '男', '2260', '306', '275', '0', '0', '46', '5', '1', '5', '', '5');
INSERT INTO `guaiwu` VALUES ('精锐叛兵', '47', '97', '精锐叛兵', '男', '2360', '320', '287', '0', '0', '47', '5', '', '5', '', '5');
INSERT INTO `guaiwu` VALUES ('叛兵队长', '50', '98', '叛兵队长', '男', '3510', '440', '405', '0', '0', '48', '5', '', '5', '', '5');

-- ----------------------------
-- Table structure for im
-- ----------------------------
DROP TABLE IF EXISTS `im`;
CREATE TABLE `im` (
  `imuid` int(11) NOT NULL,
  `sid` int(11) NOT NULL,
  PRIMARY KEY (`imuid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of im
-- ----------------------------

-- ----------------------------
-- Table structure for jineng
-- ----------------------------
DROP TABLE IF EXISTS `jineng`;
CREATE TABLE `jineng` (
  `jnname` varchar(255) NOT NULL,
  `jnid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `jngj` int(11) NOT NULL,
  `jnfy` int(11) NOT NULL,
  `jnbj` int(11) NOT NULL,
  `jnxx` int(11) NOT NULL,
  `jndj` int(11) NOT NULL,
  `djcount` int(11) NOT NULL,
  `xiaohao` int(11) NOT NULL,
  PRIMARY KEY (`jnid`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of jineng
-- ----------------------------
INSERT INTO `jineng` VALUES ('聚灵斩', '4', '10', '0', '0', '2', '6', '5', '0');
INSERT INTO `jineng` VALUES ('魔心爆发', '5', '5', '0', '10', '10', '7', '8', '0');
INSERT INTO `jineng` VALUES ('蛮力附体', '6', '7', '7', '7', '7', '9', '8', '0');
INSERT INTO `jineng` VALUES ('怒血爆', '7', '12', '2', '20', '8', '10', '10', '0');
INSERT INTO `jineng` VALUES ('初级嗜血术', '8', '1', '0', '0', '20', '11', '15', '0');

-- ----------------------------
-- Table structure for mid
-- ----------------------------
DROP TABLE IF EXISTS `mid`;
CREATE TABLE `mid` (
  `mname` text NOT NULL,
  `mid` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `mgid` text NOT NULL,
  `mnpc` text NOT NULL,
  `mgtime` datetime NOT NULL,
  `ms` int(11) NOT NULL,
  `midinfo` text NOT NULL,
  `midboss` int(11) NOT NULL,
  `mup` int(11) NOT NULL,
  `mdown` int(11) NOT NULL,
  `mleft` int(11) NOT NULL,
  `mright` int(11) NOT NULL,
  `mqy` int(11) NOT NULL,
  `playerinfo` varchar(255) NOT NULL,
  `ispvp` int(255) NOT NULL,
  PRIMARY KEY (`mid`)
) ENGINE=InnoDB AUTO_INCREMENT=274 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of mid
-- ----------------------------
INSERT INTO `mid` VALUES ('村广场', '225', '', '11,17', '0000-00-00 00:00:00', '0', '村里的广场，闲时貌似很多人都回在这里', '0', '0', '0', '0', '226', '14', '没有角色向村东 走去', '0');
INSERT INTO `mid` VALUES ('村东', '226', '', '18', '0000-00-00 00:00:00', '0', '村东处', '0', '0', '0', '225', '228', '14', '没有角色向村口[战斗] 走去', '0');
INSERT INTO `mid` VALUES ('村口[战斗]', '228', '55|3,56|2', '', '2017-01-04 14:01:30', '0', '村口，经常有怪物来袭', '0', '0', '0', '226', '229', '14', '没有角色向小树林 走去', '1');
INSERT INTO `mid` VALUES ('小树林', '229', '56|2,57|5', '', '2017-01-04 17:38:48', '0', '', '0', '0', '0', '228', '230', '14', '没有角色向树林深处 走去', '0');
INSERT INTO `mid` VALUES ('树林深处', '230', '58|4', '', '2017-01-04 17:12:15', '0', '', '0', '0', '0', '229', '231', '14', '没有角色向山林外围 走去', '0');
INSERT INTO `mid` VALUES ('山林外围', '231', '62|3,61|2', '', '2017-01-04 17:12:37', '0', '山林外围', '0', '0', '0', '230', '232', '14', '没有角色向山中湖泊 走去', '0');
INSERT INTO `mid` VALUES ('山中湖泊', '232', '63|2,64|5', '', '2017-01-04 12:20:20', '0', '山中湖泊', '0', '0', '0', '231', '233', '14', '没有角色向湖中小岛 走去', '0');
INSERT INTO `mid` VALUES ('湖中小岛', '233', '65|6', '', '2017-01-04 17:39:44', '0', '湖中小岛', '0', '0', '0', '232', '235', '14', '没有角色向村广场 走去', '0');
INSERT INTO `mid` VALUES ('山脉东出口', '235', '66|3,67|4', '', '2017-01-04 17:40:07', '0', '', '0', '0', '0', '233', '236', '14', '没有角色向聚仙城中心 走去', '0');
INSERT INTO `mid` VALUES ('城西郊', '236', '69|5', '', '2016-08-23 22:31:23', '0', '', '0', '0', '0', '235', '237', '16', '没有角色向山脉东出口 走去', '0');
INSERT INTO `mid` VALUES ('聚仙城西', '237', '', '13,14', '0000-00-00 00:00:00', '0', '', '0', '0', '0', '236', '238', '16', '没有角色向聚仙西街 走去', '0');
INSERT INTO `mid` VALUES ('聚仙西街', '238', '', '15', '0000-00-00 00:00:00', '0', '', '0', '0', '0', '237', '239', '16', '没有角色向聚仙城中心 走去', '0');
INSERT INTO `mid` VALUES ('聚仙城中心', '239', '', '16,17', '0000-00-00 00:00:00', '0', '', '0', '0', '273', '238', '240', '16', '没有角色向聚仙东街 走去', '0');
INSERT INTO `mid` VALUES ('聚仙东街', '240', '', '23', '0000-00-00 00:00:00', '0', '', '0', '0', '0', '239', '241', '16', '没有角色向聚仙城东 走去', '0');
INSERT INTO `mid` VALUES ('聚仙城东', '241', '', '', '0000-00-00 00:00:00', '0', '', '0', '0', '0', '240', '242', '16', '没有角色向城东郊 走去', '0');
INSERT INTO `mid` VALUES ('城东郊', '242', '70|3,71|2', '', '2016-08-24 02:10:07', '0', '', '0', '0', '0', '241', '243', '16', '没有角色向魔化洞窟 走去', '0');
INSERT INTO `mid` VALUES ('魔化洞窟', '243', '72|6', '', '2016-08-21 19:08:10', '0', '', '0', '246', '0', '242', '244', '16', '没有角色向洞窟出口 走去', '0');
INSERT INTO `mid` VALUES ('洞窟出口', '244', '74|5', '', '2016-08-21 19:01:07', '0', '', '0', '0', '0', '243', '245', '17', '没有角色向魔化平原 走去', '0');
INSERT INTO `mid` VALUES ('魔化平原', '245', '73|4', '', '2016-08-22 14:09:28', '0', '', '0', '0', '0', '244', '247', '17', '没有角色向部落西郊 走去', '0');
INSERT INTO `mid` VALUES ('洞窟深处', '246', '75|1', '', '2016-08-22 15:34:56', '200', '', '0', '0', '243', '0', '0', '17', '没有角色向魔化洞窟 走去', '0');
INSERT INTO `mid` VALUES ('部落西郊', '247', '76|4,77|2', '', '2016-08-23 11:52:26', '0', '', '0', '0', '0', '245', '248', '17', '没有角色向部落西 走去', '0');
INSERT INTO `mid` VALUES ('部落西', '248', '', '20', '0000-00-00 00:00:00', '0', '', '0', '0', '0', '247', '249', '17', '没有角色向部落中心 走去', '0');
INSERT INTO `mid` VALUES ('部落中心', '249', '', '15,17,21', '0000-00-00 00:00:00', '0', '', '0', '0', '0', '248', '250', '17', '没有角色向部落东 走去', '0');
INSERT INTO `mid` VALUES ('部落东', '250', '', '19,22', '0000-00-00 00:00:00', '0', '', '0', '0', '0', '249', '251', '17', '没有角色向部落东郊 走去', '0');
INSERT INTO `mid` VALUES ('部落东郊', '251', '78|6', '', '2016-08-23 15:32:59', '0', '', '0', '0', '0', '250', '252', '17', '没有角色向蛮河岸边 走去', '0');
INSERT INTO `mid` VALUES ('蛮河岸边', '252', '78|2,79|4', '', '2016-08-21 13:16:30', '0', '', '0', '0', '0', '251', '253', '17', '没有角色向渡河港口 走去', '0');
INSERT INTO `mid` VALUES ('渡河港口', '253', '80|5', '', '2016-08-20 13:51:25', '0', '', '0', '0', '0', '252', '254', '17', '没有角色向蛮河对岸 走去', '0');
INSERT INTO `mid` VALUES ('蛮河对岸', '254', '81|6', '', '2016-08-20 13:53:02', '0', '', '0', '0', '0', '253', '255', '17', '没有角色向未知山岭1 走去', '0');
INSERT INTO `mid` VALUES ('未知山岭1', '255', '82|7', '', '2016-08-22 06:55:18', '0', '', '0', '0', '0', '254', '256', '18', '没有角色向未知山岭2 走去', '0');
INSERT INTO `mid` VALUES ('未知山岭2', '256', '83|5', '', '2016-08-22 06:55:32', '30', '', '0', '257', '0', '255', '258', '18', '没有角色向未知山岭3 走去', '0');
INSERT INTO `mid` VALUES ('无人山洞', '257', '84|1', '', '2017-01-04 17:14:02', '200', '', '0', '0', '256', '0', '0', '18', '没有角色向未知山岭2 走去', '0');
INSERT INTO `mid` VALUES ('未知山岭3', '258', '85|6', '', '2016-08-21 17:26:27', '200', '', '0', '0', '0', '256', '259', '18', '没有角色向风铃古镇西 走去', '0');
INSERT INTO `mid` VALUES ('风铃古镇西', '259', '', '15', '0000-00-00 00:00:00', '0', '', '0', '0', '0', '258', '260', '18', '没有角色向风铃古镇 走去', '0');
INSERT INTO `mid` VALUES ('风铃古镇', '260', '', '24', '0000-00-00 00:00:00', '0', '', '0', '0', '0', '259', '261', '18', '没有角色向风铃古镇东 走去', '0');
INSERT INTO `mid` VALUES ('风铃古镇东', '261', '', '19', '0000-00-00 00:00:00', '0', '', '0', '0', '0', '260', '262', '18', '没有角色向妖兽丛林外部1 走去', '0');
INSERT INTO `mid` VALUES ('妖兽丛林外部1', '262', '86|3', '', '2016-08-23 14:30:22', '200', '', '0', '0', '0', '261', '263', '18', '没有角色向妖兽丛林外部2 走去', '0');
INSERT INTO `mid` VALUES ('妖兽丛林外部2', '263', '87|7', '', '2016-08-21 09:46:46', '0', '', '0', '0', '0', '262', '264', '18', '没有角色向丛林深处妖兽山 走去', '0');
INSERT INTO `mid` VALUES ('丛林深处妖兽山', '264', '88|5', '', '2016-08-23 11:54:01', '100', '', '0', '0', '0', '263', '265', '18', '没有角色向万妖山底部 走去', '0');
INSERT INTO `mid` VALUES ('万妖山底部', '265', '89|4,90|4', '', '2016-08-18 11:28:51', '0', '', '0', '0', '0', '264', '266', '18', '没有角色向万妖山腰 走去', '0');
INSERT INTO `mid` VALUES ('万妖山腰', '266', '91|3,92|5', '', '2016-08-19 10:05:38', '0', '', '0', '268', '267', '265', '269', '18', '没有角色向炎阳郊外 走去', '0');
INSERT INTO `mid` VALUES ('妖王殿外围', '267', '94|3,95|3', '24', '2016-08-22 11:58:20', '0', '', '0', '266', '0', '0', '0', '20', '没有角色向村广场 走去', '0');
INSERT INTO `mid` VALUES ('万妖山顶', '268', '93|7', '', '2016-08-23 15:34:56', '0', '', '0', '0', '266', '0', '0', '18', '没有角色向万妖山腰 走去', '0');
INSERT INTO `mid` VALUES ('炎阳郊外', '269', '96|4,97|4', '', '2016-08-22 07:01:01', '0', '', '0', '0', '0', '266', '270', '21', '没有角色向炎阳西郊 走去', '0');
INSERT INTO `mid` VALUES ('炎阳西郊', '270', '98|7', '', '2016-08-23 15:33:16', '0', '', '0', '0', '0', '269', '271', '21', '没有角色向炎阳西街 走去', '0');
INSERT INTO `mid` VALUES ('炎阳西街', '271', '', '', '0000-00-00 00:00:00', '0', '', '0', '0', '0', '270', '272', '21', '没有角色向炎阳城中心 走去', '0');
INSERT INTO `mid` VALUES ('炎阳城中心', '272', '', '24,25', '0000-00-00 00:00:00', '0', '', '0', '0', '0', '271', '0', '21', '没有角色向村广场 走去', '0');
INSERT INTO `mid` VALUES ('门派管理处', '273', '', '26', '0000-00-00 00:00:00', '0', '', '0', '239', '0', '0', '0', '16', '没有角色向聚仙城中心 走去', '0');

-- ----------------------------
-- Table structure for midguaiwu
-- ----------------------------
DROP TABLE IF EXISTS `midguaiwu`;
CREATE TABLE `midguaiwu` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `gname` text NOT NULL,
  `ghp` text NOT NULL,
  `ggj` text NOT NULL,
  `gfy` text NOT NULL,
  `glv` text NOT NULL,
  `mid` int(11) NOT NULL,
  `gyid` int(11) NOT NULL,
  `gexp` text NOT NULL,
  `sid` text NOT NULL,
  `gmaxhp` varchar(255) NOT NULL,
  `gbj` int(11) NOT NULL,
  `gxx` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2314252 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of midguaiwu
-- ----------------------------
INSERT INTO `midguaiwu` VALUES ('2313689', '魔化蛮狼', '700', '87', '71', '23', '251', '78', '150', '', '700', '0', '0');
INSERT INTO `midguaiwu` VALUES ('2305732', '棘鼠', '880', '110', '90', '29', '256', '83', '218', '', '880', '0', '0');
INSERT INTO `midguaiwu` VALUES ('2280014', '雷萝妖', '1474', '265', '238', '39', '266', '92', '254', '099f465c0c34dd5ef59f230a21447af4', '1960', '0', '0');
INSERT INTO `midguaiwu` VALUES ('2313635', '入魔蛮族战士', '640', '80', '65', '21', '247', '76', '158', '', '640', '0', '0');
INSERT INTO `midguaiwu` VALUES ('2313697', '叛兵队长', '3510', '440', '405', '50', '270', '98', '375', '', '3510', '0', '0');
INSERT INTO `midguaiwu` VALUES ('2313688', '魔化蛮狼', '700', '87', '71', '23', '251', '78', '150', '', '700', '0', '0');
INSERT INTO `midguaiwu` VALUES ('2314238', '老虎', '70', '8', '6', '2', '229', '57', '17', '', '70', '0', '0');
INSERT INTO `midguaiwu` VALUES ('2314246', '血雷鹰', '370', '46', '37', '12', '235', '66', '78', '', '370', '0', '0');
INSERT INTO `midguaiwu` VALUES ('2313771', '魔修士', '400', '49', '40', '13', '236', '69', '85', '', '400', '0', '0');
INSERT INTO `midguaiwu` VALUES ('2313690', '魔化蛮狼', '700', '87', '71', '23', '251', '78', '150', '', '700', '0', '0');
INSERT INTO `midguaiwu` VALUES ('2313692', '叛兵队长', '3510', '440', '405', '50', '270', '98', '375', '', '3510', '0', '0');
INSERT INTO `midguaiwu` VALUES ('2305736', '棘鼠', '880', '110', '90', '29', '256', '83', '218', '', '880', '0', '0');
INSERT INTO `midguaiwu` VALUES ('2305729', '赤鳞兽', '850', '106', '87', '28', '255', '82', '238', '', '850', '0', '0');
INSERT INTO `midguaiwu` VALUES ('2253163', '山猪', '40', '4', '4', '1', '228', '56', '8', '0e698c7ec2d718b658fa287c45929571', '40', '0', '0');
INSERT INTO `midguaiwu` VALUES ('2305764', '精锐叛兵', '2360', '320', '287', '47', '269', '97', '400', '', '2360', '0', '0');
INSERT INTO `midguaiwu` VALUES ('2313696', '叛兵队长', '3510', '440', '405', '50', '270', '98', '375', '', '3510', '0', '0');
INSERT INTO `midguaiwu` VALUES ('2313639', '入魔蛮族祭祀', '670', '84', '68', '22', '247', '77', '187', '', '670', '0', '0');
INSERT INTO `midguaiwu` VALUES ('2305761', '劫杀者', '2260', '306', '275', '45', '269', '96', '293', '', '2260', '0', '0');
INSERT INTO `midguaiwu` VALUES ('2297445', '赤炼蝮蛇妖', '1660', '224', '201', '33', '263', '87', '281', '', '1660', '0', '0');
INSERT INTO `midguaiwu` VALUES ('2297446', '赤炼蝮蛇妖', '1660', '224', '201', '33', '263', '87', '281', '', '1660', '0', '0');
INSERT INTO `midguaiwu` VALUES ('2302423', '魔化之鸦人', '610', '76', '62', '20', '244', '74', '170', '', '610', '0', '0');
INSERT INTO `midguaiwu` VALUES ('2302424', '魔化之鸦人', '610', '76', '62', '20', '244', '74', '170', '', '610', '0', '0');
INSERT INTO `midguaiwu` VALUES ('2302425', '魔化之鸦人', '610', '76', '62', '20', '244', '74', '170', '', '610', '0', '0');
INSERT INTO `midguaiwu` VALUES ('2305762', '劫杀者', '2260', '306', '275', '45', '269', '96', '293', '', '2260', '0', '0');
INSERT INTO `midguaiwu` VALUES ('2314182', '百岁龙雀', '250', '32', '25', '8', '232', '64', '60', '', '250', '0', '0');
INSERT INTO `midguaiwu` VALUES ('2313886', '魔化之通臂猿', '490', '61', '50', '16', '242', '70', '136', '', '490', '0', '0');
INSERT INTO `midguaiwu` VALUES ('2305728', '赤鳞兽', '850', '106', '87', '28', '255', '82', '238', '', '850', '0', '0');
INSERT INTO `midguaiwu` VALUES ('2299847', '魔化蛮狼', '700', '87', '71', '23', '252', '78', '173', '', '700', '0', '0');
INSERT INTO `midguaiwu` VALUES ('2299850', '魔化狂狮', '730', '91', '74', '24', '252', '79', '180', '', '730', '0', '0');
INSERT INTO `midguaiwu` VALUES ('2299851', '魔化狂狮', '730', '91', '74', '24', '252', '79', '180', '', '730', '0', '0');
INSERT INTO `midguaiwu` VALUES ('2314183', '百岁龙雀', '250', '32', '25', '8', '232', '64', '60', '', '250', '0', '0');
INSERT INTO `midguaiwu` VALUES ('2313887', '魔化之通臂猿', '490', '61', '50', '16', '242', '70', '136', '', '490', '0', '0');
INSERT INTO `midguaiwu` VALUES ('2314245', '血雷鹰', '370', '46', '37', '12', '235', '66', '78', '', '370', '0', '0');
INSERT INTO `midguaiwu` VALUES ('2313685', '魔化蛮狼', '700', '87', '71', '23', '251', '78', '150', '', '700', '0', '0');
INSERT INTO `midguaiwu` VALUES ('2313686', '魔化蛮狼', '700', '87', '71', '23', '251', '78', '150', '', '700', '0', '0');
INSERT INTO `midguaiwu` VALUES ('2313687', '魔化蛮狼', '700', '87', '71', '23', '251', '78', '150', '', '700', '0', '0');
INSERT INTO `midguaiwu` VALUES ('2305763', '精锐叛兵', '2360', '320', '287', '47', '269', '97', '400', '', '2360', '0', '0');
INSERT INTO `midguaiwu` VALUES ('2313645', '百年穿山甲', '2000', '150', '330', '34', '264', '88', '255', '', '2000', '0', '0');
INSERT INTO `midguaiwu` VALUES ('2314223', '花妖', '110', '15', '7', '3', '230', '58', '26', '', '110', '0', '0');
INSERT INTO `midguaiwu` VALUES ('2302033', '岩魔', '1560', '211', '189', '31', '258', '85', '202', '', '1560', '0', '0');
INSERT INTO `midguaiwu` VALUES ('2305759', '劫杀者', '2260', '306', '275', '45', '269', '96', '293', '', '2260', '0', '0');
INSERT INTO `midguaiwu` VALUES ('2305760', '劫杀者', '2260', '306', '275', '45', '269', '96', '293', '', '2260', '0', '0');
INSERT INTO `midguaiwu` VALUES ('2313694', '叛兵队长', '3510', '440', '405', '50', '270', '98', '375', '', '3510', '0', '0');
INSERT INTO `midguaiwu` VALUES ('2302037', '岩魔', '1560', '211', '189', '31', '258', '85', '202', '', '1560', '0', '0');
INSERT INTO `midguaiwu` VALUES ('2309221', '魔化之幼齿猫女', '580', '72', '59', '19', '245', '73', '124', '', '580', '0', '0');
INSERT INTO `midguaiwu` VALUES ('2290134', '百年蛇怪', '820', '103', '84', '27', '254', '81', '176', '', '820', '0', '0');
INSERT INTO `midguaiwu` VALUES ('2309219', '魔化之幼齿猫女', '580', '72', '59', '19', '245', '73', '124', '', '580', '0', '0');
INSERT INTO `midguaiwu` VALUES ('2313693', '叛兵队长', '3510', '440', '405', '50', '270', '98', '375', '', '3510', '0', '0');
INSERT INTO `midguaiwu` VALUES ('2305765', '精锐叛兵', '2360', '320', '287', '47', '269', '97', '400', '', '2360', '0', '0');
INSERT INTO `midguaiwu` VALUES ('2269220', '青古虎', '1760', '238', '214', '35', '265', '89', '263', '', '1760', '0', '0');
INSERT INTO `midguaiwu` VALUES ('2290131', '百年蛇怪', '820', '103', '84', '27', '254', '81', '176', '', '820', '0', '0');
INSERT INTO `midguaiwu` VALUES ('2313637', '入魔蛮族战士', '640', '80', '65', '21', '247', '76', '158', '', '640', '0', '0');
INSERT INTO `midguaiwu` VALUES ('2302461', '魔化之剑魔', '550', '68', '56', '18', '243', '72', '153', '', '550', '0', '0');
INSERT INTO `midguaiwu` VALUES ('2302462', '魔化之剑魔', '550', '68', '56', '18', '243', '72', '153', '', '550', '0', '0');
INSERT INTO `midguaiwu` VALUES ('2305734', '棘鼠', '880', '110', '90', '29', '256', '83', '218', '', '880', '0', '0');
INSERT INTO `midguaiwu` VALUES ('2314251', '雷鹰', '310', '38', '31', '10', '235', '67', '85', '', '310', '0', '0');
INSERT INTO `midguaiwu` VALUES ('2313640', '入魔蛮族祭祀', '670', '84', '68', '22', '247', '77', '187', '', '670', '0', '0');
INSERT INTO `midguaiwu` VALUES ('2302474', '魔化之通臂猿', '490', '61', '50', '16', '242', '70', '136', '781a121e409741ff53f5978578067146', '490', '0', '0');
INSERT INTO `midguaiwu` VALUES ('2249197', '魔化蛮狼', '700', '87', '71', '23', '251', '78', '173', 'e0e644a3727f0f1671e917f7b376c66f', '700', '0', '0');
INSERT INTO `midguaiwu` VALUES ('2309222', '魔化之幼齿猫女', '580', '72', '59', '19', '245', '73', '124', '', '580', '0', '0');
INSERT INTO `midguaiwu` VALUES ('2314180', '百岁龙雀', '250', '32', '25', '8', '232', '64', '60', '', '250', '0', '0');
INSERT INTO `midguaiwu` VALUES ('2313514', '狂暴野狼', '144', '23', '19', '6', '231', '62', '45', 'bb2a45b7652a7900e7810128a329597e', '190', '0', '0');
INSERT INTO `midguaiwu` VALUES ('2280013', '雷萝妖', '1960', '265', '238', '39', '266', '92', '254', '', '1960', '0', '0');
INSERT INTO `midguaiwu` VALUES ('2302427', '魔化之鸦人', '610', '76', '62', '20', '244', '74', '170', '', '610', '0', '0');
INSERT INTO `midguaiwu` VALUES ('2313695', '叛兵队长', '3510', '440', '405', '50', '270', '98', '375', '', '3510', '0', '0');
INSERT INTO `midguaiwu` VALUES ('2313641', '百年穿山甲', '2000', '150', '330', '34', '264', '88', '255', '', '2000', '0', '0');
INSERT INTO `midguaiwu` VALUES ('2314248', '雷鹰', '310', '38', '31', '10', '235', '67', '85', '', '310', '0', '0');
INSERT INTO `midguaiwu` VALUES ('2290121', '魔道控兽师', '790', '99', '81', '26', '253', '80', '169', '', '790', '0', '0');
INSERT INTO `midguaiwu` VALUES ('2280015', '雷萝妖', '1960', '265', '238', '39', '266', '92', '254', '', '1960', '0', '0');
INSERT INTO `midguaiwu` VALUES ('2313678', '嗜灵鼠妖', '1610', '218', '195', '32', '262', '86', '208', '', '1610', '0', '0');
INSERT INTO `midguaiwu` VALUES ('2305733', '棘鼠', '880', '110', '90', '29', '256', '83', '218', '', '880', '0', '0');
INSERT INTO `midguaiwu` VALUES ('2309220', '魔化之幼齿猫女', '580', '72', '59', '19', '245', '73', '124', '', '580', '0', '0');
INSERT INTO `midguaiwu` VALUES ('2297451', '赤炼蝮蛇妖', '1660', '224', '201', '33', '263', '87', '281', '', '1660', '0', '0');
INSERT INTO `midguaiwu` VALUES ('2313642', '百年穿山甲', '2000', '150', '330', '34', '264', '88', '255', '', '2000', '0', '0');
INSERT INTO `midguaiwu` VALUES ('2310366', '魔化山魈', '640', '80', '65', '21', '246', '75', '179', '', '640', '0', '0');
INSERT INTO `midguaiwu` VALUES ('2313679', '嗜灵鼠妖', '1610', '218', '195', '32', '262', '86', '208', '', '1610', '0', '0');
INSERT INTO `midguaiwu` VALUES ('2305727', '赤鳞兽', '850', '106', '87', '28', '255', '82', '238', '', '850', '0', '0');
INSERT INTO `midguaiwu` VALUES ('2302426', '魔化之鸦人', '610', '76', '62', '20', '244', '74', '170', '', '610', '0', '0');
INSERT INTO `midguaiwu` VALUES ('2302036', '岩魔', '1560', '211', '189', '31', '258', '85', '202', '', '1560', '0', '0');
INSERT INTO `midguaiwu` VALUES ('2308269', '黑艳妖王', '4530', '653', '454', '56', '267', '95', '420', '', '4530', '0', '0');
INSERT INTO `midguaiwu` VALUES ('2314242', '荷花花魅', '280', '35', '28', '9', '233', '65', '68', '', '280', '0', '0');
INSERT INTO `midguaiwu` VALUES ('2314241', '荷花花魅', '280', '35', '28', '9', '233', '65', '68', '', '280', '0', '0');
INSERT INTO `midguaiwu` VALUES ('2313888', '魔化之灵猴', '520', '65', '53', '17', '242', '71', '145', '', '520', '0', '0');
INSERT INTO `midguaiwu` VALUES ('2314236', '老虎', '70', '8', '6', '2', '229', '57', '17', '', '70', '0', '0');
INSERT INTO `midguaiwu` VALUES ('2313644', '百年穿山甲', '2000', '150', '330', '34', '264', '88', '255', '', '2000', '0', '0');
INSERT INTO `midguaiwu` VALUES ('2313772', '魔修士', '400', '49', '40', '13', '236', '69', '85', '', '400', '0', '0');
INSERT INTO `midguaiwu` VALUES ('2313638', '入魔蛮族战士', '640', '80', '65', '21', '247', '76', '158', '', '640', '0', '0');
INSERT INTO `midguaiwu` VALUES ('2313636', '入魔蛮族战士', '640', '80', '65', '21', '247', '76', '158', '', '640', '0', '0');
INSERT INTO `midguaiwu` VALUES ('2313710', '血鸦', '2010', '272', '244', '40', '268', '93', '260', '', '2010', '0', '0');
INSERT INTO `midguaiwu` VALUES ('2313709', '血鸦', '2010', '272', '244', '40', '268', '93', '260', '', '2010', '0', '0');
INSERT INTO `midguaiwu` VALUES ('2313691', '叛兵队长', '3510', '440', '405', '50', '270', '98', '375', '', '3510', '0', '0');
INSERT INTO `midguaiwu` VALUES ('2314247', '血雷鹰', '370', '46', '37', '12', '235', '66', '78', '', '370', '0', '0');
INSERT INTO `midguaiwu` VALUES ('2314243', '荷花花魅', '280', '35', '28', '9', '233', '65', '68', '', '280', '0', '0');
INSERT INTO `midguaiwu` VALUES ('2314226', '狂暴野狼', '190', '23', '19', '6', '231', '62', '39', '', '190', '0', '0');
INSERT INTO `midguaiwu` VALUES ('2314244', '荷花花魅', '280', '35', '28', '9', '233', '65', '68', '', '280', '0', '0');
INSERT INTO `midguaiwu` VALUES ('2314179', '百岁龙雀', '250', '32', '25', '8', '232', '64', '60', '', '250', '0', '0');
INSERT INTO `midguaiwu` VALUES ('2314228', '狂暴野狼', '190', '23', '19', '6', '231', '62', '39', '', '190', '0', '0');
INSERT INTO `midguaiwu` VALUES ('2314227', '狂暴野狼', '190', '23', '19', '6', '231', '62', '39', '', '190', '0', '0');
INSERT INTO `midguaiwu` VALUES ('2314250', '雷鹰', '310', '38', '31', '10', '235', '67', '85', '', '310', '0', '0');
INSERT INTO `midguaiwu` VALUES ('2314215', '硬翅蜂', '40', '5', '3', '1', '228', '55', '7', '', '40', '0', '0');
INSERT INTO `midguaiwu` VALUES ('2314213', '硬翅蜂', '40', '5', '3', '1', '228', '55', '7', '', '40', '0', '0');
INSERT INTO `midguaiwu` VALUES ('2314231', '怨灵', '1510', '204', '183', '30', '257', '84', '225', '', '1510', '0', '0');
INSERT INTO `midguaiwu` VALUES ('2314249', '雷鹰', '310', '38', '31', '10', '235', '67', '85', '', '310', '0', '0');

-- ----------------------------
-- Table structure for npc
-- ----------------------------
DROP TABLE IF EXISTS `npc`;
CREATE TABLE `npc` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `nname` text CHARACTER SET utf8 NOT NULL,
  `nsex` varchar(255) NOT NULL,
  `ninfo` text CHARACTER SET utf8 NOT NULL,
  `muban` text NOT NULL,
  `taskid` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of npc
-- ----------------------------
INSERT INTO `npc` VALUES ('11', '村长', '男', '村长', '', '13,25,24,28');
INSERT INTO `npc` VALUES ('13', '王老五', '男', '唉……一个人的日子，真的好难。', '', '24');
INSERT INTO `npc` VALUES ('14', '赫炳', '男', '唉，整天站在这里真是无聊。', '', '28');
INSERT INTO `npc` VALUES ('15', '周富贵[商人]', '男', '来来来   便宜', '商店.php', '');
INSERT INTO `npc` VALUES ('16', '聚仙城主[符箓]', '男', '聚仙城城主', '技能兑换.php', '');
INSERT INTO `npc` VALUES ('18', '王大妈', '女', '王大妈', '', '24,29');
INSERT INTO `npc` VALUES ('17', '云游仙医[治疗]', '男', '云游的仙医，似乎在哪都能看见他', '治疗.php', '');
INSERT INTO `npc` VALUES ('19', '符箓大师', '男', '技能大师，负责兑换技能', '技能兑换.php', '');
INSERT INTO `npc` VALUES ('20', '小蛮', '女', '小蛮好怕...', '', '20');
INSERT INTO `npc` VALUES ('21', '蛮族长老', '男', '蛮族长老', '', '19');
INSERT INTO `npc` VALUES ('22', '蛮族猎手', '男', '老了,干不动了', '', '21');
INSERT INTO `npc` VALUES ('23', '兑换大使', '男', '兑换大使', '', '27');
INSERT INTO `npc` VALUES ('24', '正规仙医', '男', '正规仙医\r\n比云游的更在行', '治疗_级别1.php', '');
INSERT INTO `npc` VALUES ('25', '城主雪琴', '女', '城炎阳城的城主,雪琴', '', '');
INSERT INTO `npc` VALUES ('26', '门派管理员', '男', '门派管理', '门派管理员.php', '');

-- ----------------------------
-- Table structure for playerchongwu
-- ----------------------------
DROP TABLE IF EXISTS `playerchongwu`;
CREATE TABLE `playerchongwu` (
  `cwid` int(11) NOT NULL AUTO_INCREMENT,
  `cwname` varchar(255) NOT NULL,
  `cwhp` int(11) NOT NULL,
  `cwmaxhp` int(11) NOT NULL,
  `cwgj` int(11) NOT NULL,
  `cwfy` int(11) NOT NULL,
  `cwbj` int(11) NOT NULL,
  `cwxx` int(11) NOT NULL,
  `cwlv` int(11) NOT NULL,
  `cwexp` int(11) NOT NULL,
  `tool1` int(11) NOT NULL,
  `tool2` int(11) NOT NULL,
  `tool3` int(11) NOT NULL,
  `tool4` int(11) NOT NULL,
  `tool5` int(11) NOT NULL,
  `tool6` int(11) NOT NULL,
  `sid` varchar(255) NOT NULL,
  `uphp` int(11) NOT NULL,
  `upgj` int(11) NOT NULL,
  `upfy` int(11) NOT NULL,
  `cwpz` int(11) NOT NULL,
  PRIMARY KEY (`cwid`)
) ENGINE=InnoDB AUTO_INCREMENT=3587 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of playerchongwu
-- ----------------------------

-- ----------------------------
-- Table structure for playerdaoju
-- ----------------------------
DROP TABLE IF EXISTS `playerdaoju`;
CREATE TABLE `playerdaoju` (
  `djname` varchar(255) NOT NULL,
  `djzl` varchar(255) NOT NULL,
  `djinfo` varchar(255) NOT NULL,
  `uid` int(11) NOT NULL,
  `sid` text NOT NULL,
  `djsum` int(11) NOT NULL,
  `djid` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of playerdaoju
-- ----------------------------

-- ----------------------------
-- Table structure for playerjineng
-- ----------------------------
DROP TABLE IF EXISTS `playerjineng`;
CREATE TABLE `playerjineng` (
  `jnname` varchar(255) NOT NULL,
  `jnid` int(11) NOT NULL,
  `jngj` int(11) NOT NULL,
  `jnfy` int(11) NOT NULL,
  `jnbj` int(11) NOT NULL,
  `jnxx` int(11) NOT NULL,
  `sid` text NOT NULL,
  `jncount` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of playerjineng
-- ----------------------------

-- ----------------------------
-- Table structure for playerrenwu
-- ----------------------------
DROP TABLE IF EXISTS `playerrenwu`;
CREATE TABLE `playerrenwu` (
  `rwname` varchar(255) NOT NULL,
  `rwzl` int(11) NOT NULL,
  `rwdj` varchar(255) NOT NULL,
  `rwzb` varchar(255) NOT NULL,
  `rwexp` varchar(255) NOT NULL,
  `rwyxb` varchar(255) NOT NULL,
  `sid` text NOT NULL,
  `rwzt` int(11) NOT NULL,
  `rwid` int(11) NOT NULL,
  `rwyq` int(11) NOT NULL,
  `rwcount` int(11) NOT NULL,
  `rwnowcount` int(11) NOT NULL,
  `rwlx` int(11) NOT NULL,
  `rwyp` text NOT NULL,
  `data` int(11) NOT NULL,
  `rwjineng` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of playerrenwu
-- ----------------------------

-- ----------------------------
-- Table structure for playeryaopin
-- ----------------------------
DROP TABLE IF EXISTS `playeryaopin`;
CREATE TABLE `playeryaopin` (
  `ypname` varchar(255) NOT NULL,
  `ypid` int(11) NOT NULL,
  `yphp` int(11) NOT NULL,
  `ypgj` int(11) NOT NULL,
  `ypfy` int(11) NOT NULL,
  `ypxx` int(11) NOT NULL,
  `ypbj` int(11) NOT NULL,
  `sid` text NOT NULL,
  `ypsum` int(11) NOT NULL,
  `ypjg` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of playeryaopin
-- ----------------------------

-- ----------------------------
-- Table structure for playerzhuangbei
-- ----------------------------
DROP TABLE IF EXISTS `playerzhuangbei`;
CREATE TABLE `playerzhuangbei` (
  `zbname` varchar(255) NOT NULL,
  `zbinfo` varchar(255) NOT NULL,
  `zbgj` varchar(255) NOT NULL,
  `zbfy` varchar(255) NOT NULL,
  `zbbj` varchar(255) NOT NULL,
  `zbxx` varchar(255) NOT NULL,
  `zbid` int(11) NOT NULL,
  `uid` int(11) NOT NULL,
  `zbnowid` int(11) NOT NULL AUTO_INCREMENT,
  `sid` text NOT NULL,
  `zbhp` varchar(255) NOT NULL,
  `qianghua` int(11) NOT NULL,
  `zblv` int(11) NOT NULL,
  `zbtool` int(11) NOT NULL,
  PRIMARY KEY (`zbnowid`)
) ENGINE=InnoDB AUTO_INCREMENT=75624 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of playerzhuangbei
-- ----------------------------

-- ----------------------------
-- Table structure for qy
-- ----------------------------
DROP TABLE IF EXISTS `qy`;
CREATE TABLE `qy` (
  `qyid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `qyname` varchar(255) NOT NULL,
  `mid` int(11) NOT NULL,
  PRIMARY KEY (`qyid`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of qy
-- ----------------------------
INSERT INTO `qy` VALUES ('14', '新手村', '225');
INSERT INTO `qy` VALUES ('16', '聚仙城', '239');
INSERT INTO `qy` VALUES ('17', '魔化区域', '249');
INSERT INTO `qy` VALUES ('18', '万千山域', '260');
INSERT INTO `qy` VALUES ('20', '妖王殿', '267');
INSERT INTO `qy` VALUES ('21', '炎阳城', '272');

-- ----------------------------
-- Table structure for renwu
-- ----------------------------
DROP TABLE IF EXISTS `renwu`;
CREATE TABLE `renwu` (
  `rwname` varchar(255) NOT NULL,
  `rwzl` int(11) NOT NULL,
  `rwdj` varchar(255) NOT NULL,
  `rwzb` varchar(255) NOT NULL,
  `rwexp` varchar(255) NOT NULL,
  `rwyxb` varchar(255) NOT NULL,
  `rwid` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `rwyq` int(11) NOT NULL,
  `rwinfo` varchar(255) NOT NULL,
  `rwcount` int(11) NOT NULL,
  `rwlx` int(255) NOT NULL,
  `rwyp` text NOT NULL,
  `lastrwid` int(11) NOT NULL,
  `rwjineng` varchar(255) NOT NULL,
  PRIMARY KEY (`rwid`)
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of renwu
-- ----------------------------
INSERT INTO `renwu` VALUES ('山猪祸乱', '2', '1|5', '24', '100', '120', '13', '56', '最近山猪下山,扰乱了我们的生活,请帮我们赶跑他们', '5', '2', '6|3', '0', '');
INSERT INTO `renwu` VALUES ('收集蜂蜜', '1', '1|5', '23', '200', '100', '14', '8', '收集硬翅蜂的蜂蜜', '10', '2', '6|3', '0', '');
INSERT INTO `renwu` VALUES ('蛮!', '2', '1|30,9|50', '38', '500', '500', '19', '76', '魔道对这片大地始终不死心,诱惑了我们很多族人入魔了,希望你能够解救他们', '50', '1', '', '-1', '');
INSERT INTO `renwu` VALUES ('杀!', '2', '1|50,9|50', '39', '600', '400', '20', '77', '我好怕,帮我杀了他们!!!', '50', '1', '', '-1', '');
INSERT INTO `renwu` VALUES ('赤鳞兽皮', '2', '1|10,10|50', '39', '800', '350', '21', '82', '部落现在缺少大量兽皮过冬', '30', '2', '', '-1', '');
INSERT INTO `renwu` VALUES ('硬翅蜂扰', '2', '1|15,6|100,7|100', '', '200', '150', '25', '55', '硬翅蜂扰', '20', '3', '', '-1', '');
INSERT INTO `renwu` VALUES ('找王大妈', '3', '1|20', '25', '200', '100', '24', '11', '找王大妈', '18', '3', '6|10', '25', '');
INSERT INTO `renwu` VALUES ('故人', '3', '1|50', '29', '400', '200', '28', '11', '故人', '14', '1', '6|10', '-1', '');
INSERT INTO `renwu` VALUES ('屠尽妖王', '1', '', '45', '2000', '2000', '27', '12', '屠尽妖王', '150', '1', '9|5', '-1', '');
INSERT INTO `renwu` VALUES ('狼患', '2', '1|100', '', '400', '300', '29', '62', '狼患成灾，帮帮我们', '10', '3', '', '24', '');

-- ----------------------------
-- Table structure for userinfo
-- ----------------------------
DROP TABLE IF EXISTS `userinfo`;
CREATE TABLE `userinfo` (
  `username` text,
  `userpass` text,
  `token` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of userinfo
-- ----------------------------


-- ----------------------------
-- Table structure for yaopin
-- ----------------------------
DROP TABLE IF EXISTS `yaopin`;
CREATE TABLE `yaopin` (
  `ypname` varchar(255) NOT NULL,
  `ypid` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `yphp` int(11) NOT NULL,
  `ypgj` int(11) NOT NULL,
  `ypfy` int(11) NOT NULL,
  `ypxx` int(11) NOT NULL,
  `ypbj` int(11) NOT NULL,
  `ypjg` int(11) NOT NULL,
  PRIMARY KEY (`ypid`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of yaopin
-- ----------------------------
INSERT INTO `yaopin` VALUES ('还原丹', '6', '100', '0', '0', '0', '0', '30');
INSERT INTO `yaopin` VALUES ('回血散', '7', '300', '0', '0', '0', '0', '80');
INSERT INTO `yaopin` VALUES ('回春汤', '8', '600', '0', '0', '0', '0', '155');
INSERT INTO `yaopin` VALUES ('复伤丹', '9', '1200', '0', '0', '0', '0', '310');

-- ----------------------------
-- Table structure for zhuangbei
-- ----------------------------
DROP TABLE IF EXISTS `zhuangbei`;
CREATE TABLE `zhuangbei` (
  `zbname` varchar(255) NOT NULL,
  `zbinfo` varchar(255) NOT NULL,
  `zbgj` varchar(255) NOT NULL,
  `zbfy` varchar(255) NOT NULL,
  `zbbj` varchar(255) NOT NULL,
  `zbxx` varchar(255) NOT NULL,
  `zbid` int(11) NOT NULL AUTO_INCREMENT,
  `zbhp` varchar(255) NOT NULL,
  `zblv` int(11) NOT NULL,
  `zbtool` int(11) NOT NULL,
  PRIMARY KEY (`zbid`)
) ENGINE=InnoDB AUTO_INCREMENT=49 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of zhuangbei
-- ----------------------------
INSERT INTO `zhuangbei` VALUES ('新手木剑', '新手使用的木剑', '1', '0', '0', '1', '23', '0', '0', '1');
INSERT INTO `zhuangbei` VALUES ('新手布衣', '新手使用的布衣', '0', '2', '0', '0', '24', '10', '0', '3');
INSERT INTO `zhuangbei` VALUES ('明月之剑', '明月  明月', '3', '0', '0', '1', '25', '0', '0', '1');
INSERT INTO `zhuangbei` VALUES ('清风护甲', '取自清风常伴', '0', '5', '1', '0', '26', '25', '0', '3');
INSERT INTO `zhuangbei` VALUES ('百炼青刚剑', '百炼青刚剑', '5', '0', '0', '2', '27', '0', '0', '1');
INSERT INTO `zhuangbei` VALUES ('百炼轻蕊甲', '百炼轻蕊甲', '0', '8', '0', '0', '28', '40', '0', '3');
INSERT INTO `zhuangbei` VALUES ('初级嗜血剑', '初级嗜血剑', '2', '0', '1', '3', '29', '0', '0', '1');
INSERT INTO `zhuangbei` VALUES ('轻蕊盔', '轻蕊盔', '0', '7', '1', '0', '30', '50', '0', '2');
INSERT INTO `zhuangbei` VALUES ('雷鹰护甲', '雷鹰护甲', '0', '8', '1', '0', '31', '55', '0', '3');
INSERT INTO `zhuangbei` VALUES ('血鹰项链', '血鹰项链', '0', '3', '3', '5', '32', '20', '0', '0');
INSERT INTO `zhuangbei` VALUES ('黑魔匕首', '黑魔匕首', '14', '0', '3', '4', '33', '0', '0', '0');
INSERT INTO `zhuangbei` VALUES ('中级噬血剑', '中级噬血剑', '15', '0', '0', '4', '34', '0', '0', '0');
INSERT INTO `zhuangbei` VALUES ('普通蛮甲', '普通蛮甲', '0', '9', '2', '0', '35', '62', '0', '0');
INSERT INTO `zhuangbei` VALUES ('陨铁武棍', '陨铁武棍', '8', '3', '1', '1', '36', '0', '0', '0');
INSERT INTO `zhuangbei` VALUES ('月轮枪', '月轮枪', '10', '0', '0', '2', '37', '0', '0', '0');
INSERT INTO `zhuangbei` VALUES ('厚土甲', '厚土甲', '0', '10', '1', '0', '38', '120', '0', '0');
INSERT INTO `zhuangbei` VALUES ('嗜魂骨忍', '嗜魂骨忍', '17', '0', '5', '3', '39', '0', '0', '0');
INSERT INTO `zhuangbei` VALUES ('百斩狂澜枪', '百斩狂澜', '20', '0', '0', '5', '40', '0', '0', '0');
INSERT INTO `zhuangbei` VALUES ('缘风·虬雷衣', '缘风·虬雷衣', '0', '10', '0', '0', '41', '150', '0', '0');
INSERT INTO `zhuangbei` VALUES ('缘风·墨魂靴', '缘风·墨魂靴', '0', '10', '3', '0', '42', '155', '0', '0');
INSERT INTO `zhuangbei` VALUES ('缘风·破军腰带', '缘风·破军腰带', '0', '14', '0', '0', '43', '170', '0', '0');
INSERT INTO `zhuangbei` VALUES ('缘风·兽魂项链', '缘风·兽魂项链', '18', '12', '4', '4', '44', '55', '0', '0');
INSERT INTO `zhuangbei` VALUES ('[神器]妖王剑', '[神器]妖王剑\r\n妖王剑碎片合成', '45', '0', '13', '11', '45', '0', '0', '0');
INSERT INTO `zhuangbei` VALUES ('劫刀', '劫刀', '25', '0', '4', '5', '46', '0', '0', '0');
INSERT INTO `zhuangbei` VALUES ('军用锁子甲', '军用锁子甲', '5', '16', '5', '0', '47', '170', '0', '0');
INSERT INTO `zhuangbei` VALUES ('军官陌刀', '军官陌刀', '30', '0', '5', '4', '48', '0', '0', '0');

-- ----------------------------
-- Table structure for zhurenwu
-- ----------------------------
DROP TABLE IF EXISTS `zhurenwu`;
CREATE TABLE `zhurenwu` (
  `zrwname` varchar(255) NOT NULL,
  `zrwid` int(11) NOT NULL,
  `zrwyq` varchar(255) NOT NULL,
  `yqcount` varchar(255) NOT NULL,
  `zrwjldj` varchar(255) NOT NULL,
  `zrwjlzb` varchar(255) NOT NULL,
  `zrwjlyp` varchar(255) NOT NULL,
  `zrwjljn` varchar(255) NOT NULL,
  `lastid` int(11) NOT NULL,
  `zrwexp` int(11) NOT NULL,
  `zrwyxb` int(11) NOT NULL,
  PRIMARY KEY (`zrwid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of zhurenwu
-- ----------------------------
