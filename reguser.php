<?php
include 'pdo.php';
$a = '欢迎来到紫鼎圣宫';
if (isset($_POST['submit']) && $_POST['submit']) {
	$username = $_POST['username'];
	$userpass = $_POST['userpass'];
	$userpass2 = $_POST['userpass2'];
	$username = htmlspecialchars($username);
	$userpass = htmlspecialchars($userpass);
	$sql = "select * from userinfo where username=?";
	$stmt = $dblj->prepare($sql);
	$stmt->execute(array($username));
	$stmt->bindColumn('username', $cxusername);
	$ret = $stmt->fetch(PDO::FETCH_ASSOC);
	if (empty($_POST['agreement'])) {
		$a = '您必须勾选同意用户协议才能注册';
	} else if ($userpass2 != $userpass) {
		$a = '两次输入密码不一致';
	} elseif (strlen($username) < 6 or strlen($userpass) < 6) {
		$a = '账号或密码长度请大于或等于6位';
	} elseif ($ret) {
		$a = '注册失败，账号 ' . $cxusername . ' 已经存在';
	} else {
		$token = md5("$username.$userpass" . strtotime(date('Y-m-d H:i:s')));
		$sql = "insert into userinfo(username,userpass,token) values('$username','$userpass','$token')";
		$cxjg = $dblj->exec($sql);
		$a = '注册成功';
		header("refresh:1;url=index.php");
	}
}

?>
<html lang="zh">

<head>
	<meta charset="utf-8" content="width=device-width,user-scalable=no" name="viewport">
	<title>紫鼎圣宫</title>
	<link href="//unpkg.com/layui@2.8.8/dist/css/layui.css" rel="stylesheet">
	<script src="//unpkg.com/layui@2.8.8/dist/layui.js"></script>
	<link rel="stylesheet" href="css/gamecss.css?version=1.01">
	<style>
		.demo-reg-other .layui-icon {
			position: relative;
			display: inline-block;
			margin: 0 2px;
			top: 2px;
			font-size: 26px;
		}

		.layui-text a:not(.layui-btn) {
			color: #01aaed;
		}
	</style>
</head>

<body>
	<div class="layui-container">
		<div class="layui-card">
			<div class="layui-card-body">
				<img src="images/11.jpg" width="100%" height="200">
				<br /><br />
				<div id="mainfont">
					<p>天下风云出我辈，一入江湖岁月催。</p>
					<p>皇图霸业谈笑中，不胜人生一场醉。</p>
					<p>提剑跨骑挥鬼雨，白骨如山鸟惊飞。</p>
					<p>尘事如潮人如水，只叹江湖几人回。</p>
				</div>
				<br />
				<form action="<?php echo $_SERVER['PHP_SELF'] ?>" method="post" class="layui-form">

					<div class="layui-form-item">
						<div class="layui-input-wrap">
							<div class="layui-input-prefix">
								<i class="layui-icon layui-icon-username"></i>
							</div>
							<input type="text" name="username" value="" required lay-verify="required" placeholder="账号" lay-reqtext="请填写账号" autocomplete="off" class="layui-input" lay-affix="clear">
						</div>
					</div>

					<div class="layui-form-item">
						<div class="layui-input-wrap">
							<div class="layui-input-prefix">
								<i class="layui-icon layui-icon-password"></i>
							</div>
							<input type="password" name="userpass" value="" required lay-verify="required" placeholder="密码" lay-reqtext="请填写密码" autocomplete="off" class="layui-input" lay-affix="eye">
						</div>
					</div>

					<div class="layui-form-item">
						<div class="layui-input-wrap">
							<div class="layui-input-prefix">
								<i class="layui-icon layui-icon-password"></i>
							</div>
							<input type="password" name="userpass2" value="" required lay-verify="required|confirmPassword" placeholder="确认密码" autocomplete="off" class="layui-input" lay-affix="eye">
						</div>
					</div>

					<div class="layui-form-item">
						<input type="checkbox" value="1" name="agreement" lay-verify="required" lay-skin="primary" title="同意">
						<a href="#terms" target="_blank" style="position: relative; top: 6px; left: -15px;">
							<ins>用户协议</ins>
						</a>
					</div>

					<div class="layui-form-item">
						<input class="layui-btn layui-btn-fluid" type="submit" lay-filter="demo-reg" name="submit" value="注册">
						<!-- <button class="layui-btn layui-btn-fluid" lay-submit lay-filter="demo-reg">注册</button> -->
					</div>

					<div class="layui-form-item demo-reg-other">
						<label>社交账号注册</label>
						<span style="padding: 0 21px 0 6px;">
							<a href="javascript:;"><i class="layui-icon layui-icon-login-qq" style="color: #3492ed;"></i></a>
							<a href="javascript:;"><i class="layui-icon layui-icon-login-wechat" style="color: #4daf29;"></i></a>
							<a href="javascript:;"><i class="layui-icon layui-icon-login-weibo" style="color: #cf1900;"></i></a>
						</span>
						<a href="index.php">登录已有帐号</a>
					</div>
					<!-- 
					账号：<br />
					<input type="text" name="username"><br />
					密码：<br />
					<input type="password" name="userpass"><br />
					确认密码：<br />
					<input type="password" name="userpass2"><br />
					<p>
						<input type="submit" name="submit" value="注册">
						<a href="index.php" id="btn">登陆</a>
					</p> -->
				</form>
				<footer><?php echo date('Y-m-d H:i:s') ?></footer>
			</div>
		</div>
	</div>
	<script>
		layui.layer.alert('<?php echo $a ?>');
		layui.use(function() {
			var $ = layui.$;
			var form = layui.form;
			var layer = layui.layer;
			var util = layui.util;

			// 自定义验证规则
			form.verify({
				// 确认密码
				confirmPassword: function(value, item) {
					var passwordValue = $('#reg-password').val();
					if (value !== passwordValue) {
						return '两次密码输入不一致';
					}
				}
			});

			// 提交事件
			form.on('submit(demo-reg)', function(data) {
				var field = data.field; // 获取表单字段值

				// 是否勾选同意
				if (!field.agreement) {
					layer.msg('您必须勾选同意用户协议才能注册');
					return false;
				}

				// 显示填写结果，仅作演示用
				// layer.alert(JSON.stringify(field), {
				// 	title: '当前填写的字段值'
				// });

				// 此处可执行 Ajax 等操作
				// …

				// return false; // 阻止默认 form 跳转
			});

			// 普通事件
			util.on('lay-on', {
				// 获取验证码
				'reg-get-vercode': function(othis) {
					var isvalid = form.validate('#reg-cellphone'); // 主动触发验证，v2.7.0 新增 
					// 验证通过
					if (isvalid) {
						layer.msg('手机号规则验证通过');
						// 此处可继续书写「发送验证码」等后续逻辑
						// …
					}
				}
			});
		});
	</script>
</body>

</html>