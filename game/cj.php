<?php
$selfym = $_SERVER['PHP_SELF'];
?>
<form action="<?= $selfym ?>" method="get" class="layui-form">
	<input type="hidden" name="cmd" value="cjplayer">
	<input type="hidden" name="token" value='<?= $_GET['token'] ?>'>
	<div class="layui-form-item">
		<label class="layui-form-label">角色名称</label>
		<div class="layui-input-block">
			<input type="text" name="username" autocomplete="off" placeholder="请填写角色名称" required class="layui-input">
		</div>
	</div>
	<div class="layui-form-item" pane>
		<label class="layui-form-label">性别</label>
		<div class="layui-input-block">
			<input type="radio" name="sex" value="1" title="男" checked>
			<input type="radio" name="sex" value="2" title="女">
		</div>
	</div>
	<!-- <p>
		<label>男：<input type="radio" name="sex" value="1" checked></label>
		<label>女：<input type="radio" name="sex" value="2"></label>
	</p> -->
	<div class="layui-form-item">
		<input  class="layui-btn layui-btn-fluid" type="submit" value="创建">
		<!-- <button class="layui-btn layui-btn-fluid" lay-submit lay-filter="demo-login">登录</button> -->
	</div>
	<!-- <input type="submit" value="创建"> -->
</form>