<?php
function layui_card($title = null, $content)
{
?>
	<div class="layui-card">
		<?php
		if ($title) echo '<div class="layui-card-header">' . $title . '</div>';
		?>
		<div class="layui-card-body">
			<?php
			if (is_string($content)) {
				echo $content;
			} else {
				$content();
			}
			?>
		</div>
	</div>
<?php
}
